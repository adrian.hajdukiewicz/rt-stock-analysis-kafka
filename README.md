# Real time stock analysis with Kafka

## Overview

This is your new Kedro project, which was generated using `Kedro 0.17.4`.

Take a look at the [Kedro documentation](https://kedro.readthedocs.io) to get started.

## Kafka
An instruction for ubuntu <br>
1. Install java 
```
sudo apt update
sudo apt install openjdk-8-jdk
```
Do not forget to choose the right version if there are multiple of them installed


2. Install Kafka <br>
```
# Download the file
wget https://archive.apache.org/dist/kafka/0.8.2.0/kafka_2.10-0.8.2.0.tgz

# Extract the files
tar -xzf kafka_2.10-0.8.2.0.tgz

# Move to the Kafka directory
cd kafka_2.10-0.8.2.0
```
3. Start servers
```
# Start the Zookeeper server
bin/zookeeper-server-start.sh config/zookeeper.properties

# Start the Kafka server
bin/kafka-server-start.sh config/server.properties
```
4. Run notebooks/kafka_producer.ipynb
5. `kedro run --pipeline training`
6. `kedro run --pipeline prediction` # this is consumer

## Mlflow server

```
mlflow server \
--backend-store-uri sqlite:///mlflow.db \
--default-artifact-root file:///home/adrian/mlflow \
--port 5000 \
--host 0.0.0.0
```

## Rules and guidelines

In order to get the best out of the template:

* Don't remove any lines from the `.gitignore` file we provide
* Make sure your results can be reproduced by following a [data engineering convention](https://kedro.readthedocs.io/en/stable/12_faq/01_faq.html#what-is-data-engineering-convention)
* Don't commit data to your repository
* Don't commit any credentials or your local configuration to your repository. Keep all your credentials and local configuration in `conf/local/`
* Use only `KEDRO_ENV` to define the environment (not `kedro run --env`)
* Do not use `--config` and `--params` options simultaneously when using `kedro run` because configuration passed via `--config` will most likely be skipped (it is probably a bug in kedro)



## How to install dependencies

These dependencies work well using linux. Macbooks may not handle it.

```
conda create --name <env_name> python=3.8
conda activate <env_name>
```

To install dependencies, run:

```
pip install -r src/requirements.txt
```

Alternatively, if you already have kedro installed, you may try:

```
kedro install
```

## How to run your Kedro pipeline

You can run your Kedro pipeline with:
```
kedro run --pipeline <pipeline_name>
```

or

```
kedro run --pipeline <pipeline_name> --config <path_to_config>
```

## How to test your Kedro project

Have a look at the file `src/tests/test_run.py` for instructions on how to write your tests. You can run your tests as follows:

```
kedro test
```

To configure the coverage threshold, go to the `.coveragerc` file.

## Updating project dependencies


Declare any dependencies in `src/requirements.in` for `pip` installation.<br>

They must be compiled into `requirements.txt` during pre-commit (push stage). <br><br>

(be careful, kedro build-reqs was not tested with this project; pre-commit way is recommended!)<br>Alternatively (instead of pre-commit), you may generate or update the dependency requirements for your project using:

```
kedro build-reqs
```

This will copy the contents of `src/requirements.in` into a new file `src/requirements.txt` which will be used as the source for `pip-compile`. You can see the output of the resolution by opening `src/requirements.txt`.

After this, if you'd like to update your project requirements, please update `src/requirements.in` and re-run `kedro build-reqs`.

[Further information about project dependencies](https://kedro.readthedocs.io/en/stable/04_kedro_project_setup/01_dependencies.html#project-specific-dependencies)

## How to work with Kedro and notebooks

> Note: Using `kedro jupyter` or `kedro ipython` to run your notebook provides these variables in scope: `context`, `catalog`, and `startup_error`.
>
> Jupyter, JupyterLab, and IPython are already included in the project requirements by default, so once you have run `kedro install` you will not need to take any extra steps before you use them.

### How to install `pre-commit` checks
To be able to run automatic checks before you commit the code run in git root:
```
pre-commit install
pre-commit install --hook-type commit-msg
pre-commit install --install-hooks --overwrite --hook-type pre-push
```

### How to run `pre-commit` checks for all files
To run pre-commit without commiting/pushing run in the git root:
```
pre-commit run --hook-stage push --all-files
```

### Jupyter
To use Jupyter notebooks in your Kedro project, you need to install Jupyter:

```
pip install jupyter
```

After installing Jupyter, you can start a local notebook server:

```
kedro jupyter notebook
```

### JupyterLab
To use JupyterLab, you need to install it:

```
pip install jupyterlab
```

You can also start JupyterLab:

```
kedro jupyter lab
```

### IPython
And if you want to run an IPython session:

```
kedro ipython
```

### How to convert notebook cells to nodes in a Kedro project
You can move notebook code over into a Kedro project structure using a mixture of [cell tagging](https://jupyter-notebook.readthedocs.io/en/stable/changelog.html#release-5-0-0) and Kedro CLI commands.

By adding the `node` tag to a cell and running the command below, the cell's source code will be copied over to a Python file within `src/<package_name>/nodes/`:

```
kedro jupyter convert <filepath_to_my_notebook>
```
> *Note:* The name of the Python file matches the name of the original notebook.

Alternatively, you may want to transform all your notebooks in one go. Run the following command to convert all notebook files found in the project root directory and under any of its sub-folders:

```
kedro jupyter convert --all
```

### How to ignore notebook output cells in `git`
To automatically strip out all output cell contents before committing to `git`, you can run `kedro activate-nbstripout`. This will add a hook in `.git/config` which will run `nbstripout` before anything is committed to `git`.

> *Note:* Your output cells will be retained locally.

## Package your Kedro project

[Further information about building project documentation and packaging your project](https://kedro.readthedocs.io/en/stable/03_tutorial/05_package_a_project.html)
