import logging
from typing import Union

import pandas as pd
import sklearn.pipeline


def drop_cols(
    cols_to_drop: list, data_set: pd.DataFrame, log_warnings: bool = False
) -> pd.DataFrame:
    """
    This function drops columns in every DF within data_sets argument.

    Args:
        cols_to_drop (list):  List of columns to be dropped
        log_warnings (bool): it true then info on columns selected to be dropped
            that were NOT present in the data set would be logged.

    Returns:
        Union[pd.DataFrame, Tuple]: single DataFrame with dropped columns or
            tuple containing DataFrames with dropped columns (same order as inputs).
    """
    for col in cols_to_drop:
        if col in data_set.columns:
            data_set.drop(columns=[col], inplace=True)
        else:
            if log_warnings:
                log = logging.getLogger(__name__)
                log.info(
                    f"Column (name: {col}) selected to be dropped, "
                    "was not present in the data set."
                )

    return data_set


def fit_preprocessing_pipe(tr_data: pd.DataFrame) -> sklearn.pipeline.Pipeline:
    """
    This function creates a sklearn pipeline for preprocessing
        and fits it on train data set.

    Args:
        tr_data (pd.DataFrame): Train data set (X and y columns only).

    Returns:
        (sklearn.pipeline.Pipeline): Fitted pipeline (using train).
    """
    pipe = sklearn.pipeline.Pipeline([("passthrough", None)])
    # example of not empty pipeline for testing purposes:
    # from sklearn.preprocessing import FunctionTransformer
    # pipe = sklearn.pipeline.Pipeline([("name", FunctionTransformer(np.log1p))])

    # convert dfs to arrays and add columns after preprocessing to make sure
    # columns would not be gone
    pipe.fit(tr_data)
    return pipe


def transform_preprocessing(
    pipe: sklearn.pipeline.Pipeline, data_to_transform: pd.DataFrame
) -> pd.DataFrame:
    """
    This function transforms a data set using passed (fitted) sklearn
        pipeline for preprocessing.

    Args:
        data_to_transform (pd.DataFrame): Data to be transformed

    Returns:
        pd.DataFrame, pd.DataFrame: Data set after preprocessing.
    """

    return pipe.transform(data_to_transform)


def check_duplicated_cols_merge(
    merge_on: list, df_left: pd.DataFrame, df_right: pd.DataFrame
) -> pd.DataFrame:
    """
    This function checks if there are duplicates in DataFrames that are
        about to be joined (how='Left'). It there are duplicates, they are dropped
        from the right DataFrame and a proper message shows in the log.

    Args:
        merge_on (list): List of strings that instructs
            what columns to merge on.
        df_left (pd.DataFrame): Left table.
        df_right (pd.DataFrame): Right table.

    Returns:
        pd.DataFrame: Merged DataFrame.
    """
    # drop repeated columns (except merge_on) to avoid errors
    for col in df_right.columns:
        if col in df_left.columns and col not in merge_on:
            df_right.drop(columns=[col], inplace=True)
            log = logging.getLogger(__name__)
            log.info(
                f"""Dropped \"{col}\" in right DataFrame because
                the column occurred in both left and right DataFrames and was not
                in the list of columns to merge on."""
            )
    return df_left.merge(df_right, how="left", on=merge_on)


def merge_tables_left(
    merge_on: Union[list, str],
    df_left: pd.DataFrame,
    *l_dfs_right: pd.DataFrame,
    **dict_dfs_right: pd.DataFrame,
) -> pd.DataFrame:
    """
    This function merges DataFrames (left merge). l_dfs_right are joined as
        right DFs in the order there were passed to the function.
        l_dfs_right and dict_dfs_right are right tables (in this order);
        dict_dfs_right should be passed with random keys (parameter names).

    Args:
        merge_on (Union[list, str]): single string (can be used for multiple right
        tables) or
        list of: strings or lists containing strings instructing
            on how to perform each join, e.g.
            [[merge_on_left: str, merge_on_right: str],
            merge_on_left3: str]
        df_left (pd.DataFrame): first left DataFrame to be joined

    Returns:
        pd.DataFrame: Merged DF.
    """

    for count, df_right in enumerate(
        list(l_dfs_right) + [i for i in dict_dfs_right.values()]
    ):
        # make sure column names (str) to merge on are in a list
        if isinstance(merge_on, str):
            # when merging on a single string:
            current_merge_on = [merge_on]
        elif isinstance(merge_on[count], list):
            # no changes when instructions how to merge on are already in a list
            # (merge_on is a list of lists with strings)
            current_merge_on = merge_on[count]
        else:
            # insert the single string into a list to make it compatible with
            # check_duplicated_cols_merge node
            # (when merge_on is a list of strings)
            current_merge_on = [merge_on[count]]

        df_left = check_duplicated_cols_merge(current_merge_on, df_left, df_right)

    return df_left


def replace_false_true_with_0_1(df: pd.DataFrame, cols_list: list) -> pd.DataFrame:
    """
    This function replaces boolean cols (True/False) with 1/0 values.

    Args:
        df (pd.DataFrame): DataFrame to be changed
        cols_list (list): Columns where True/False will be replaced with 1/0

    Returns:
        pd.DataFrame: Modified DataFrame
    """
    for col in cols_list:
        df[col].replace(False, 0, inplace=True)
        df[col].replace(True, 1, inplace=True)

    return df


def detect_and_replace_bool_cols_with_0_1(df: pd.DataFrame) -> pd.DataFrame:
    """
    This function replaces boolean cols (True/False) with 1/0 values.
        It automatically finds columns with bool type.

    Args:
        df (pd.DataFrame): DF to be changed

    Returns:
        pd.DataFrame: DF with 0/1 cols instead of 0/1.
    """
    return replace_false_true_with_0_1(
        df, [col for col in df.columns if df.dtypes[col] == bool]
    )


def generate_dummies(df: pd.DataFrame, dummy_cols: str) -> pd.DataFrame:
    """
    This function generates dummy variables for specified column

    Args:
        df (pd.DataFrame): DataFrame that will be modified.
        dummy_cols (list): Keyword arg. Column that will be used to generate dummies.

    Returns:
        pd.DataFrame: Modified DataFrame (with dummies but
            without original dummy_cols)
    """
    return pd.get_dummies(df, columns=dummy_cols)  # kwargs["dummy_cols"])
