from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.preprocessing.nodes import (
    drop_cols,
    fit_preprocessing_pipe,
    transform_preprocessing,
)

preprocessing = Pipeline(
    [
        node(
            func=drop_cols,
            inputs=["params:features_blacklisted", "tr_data_temp"],
            outputs="tr_data",
        ),
        node(
            func=drop_cols,
            inputs=["params:features_blacklisted", "tt_data_temp"],
            outputs="tt_data",
        ),
        node(
            func=fit_preprocessing_pipe,
            inputs=["tr_data"],
            outputs="pipe_preprocessing",
        ),
        node(
            func=transform_preprocessing,
            inputs=["pipe_preprocessing", "tr_data"],
            outputs="tr_data_preproc",
        ),
        node(
            func=transform_preprocessing,
            inputs=["pipe_preprocessing", "tt_data"],
            outputs="tt_data_preproc",
        ),
    ]
)
