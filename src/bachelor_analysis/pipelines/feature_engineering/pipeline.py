from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.feature_engineering.nodes import (
    add_lag,
    convert_datetime_to_unix,
)

feature_engineering = Pipeline(
    [
        node(
            func=convert_datetime_to_unix,
            inputs={
                "df": "raw_data",
            },
            outputs="df_unix",
        ),
        node(
            func=add_lag,
            inputs={
                "df": "df_unix",
            },
            outputs="df_lag",
        ),
    ]
)
