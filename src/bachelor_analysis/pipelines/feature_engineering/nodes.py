import time
from functools import reduce
from typing import Any, List, Tuple

import numpy as np
import pandas as pd

from bachelor_analysis.pipelines.consts import AVG_COL, LAG_COL, TIME_VAR_COL, Y_COL
from bachelor_analysis.pipelines.functools.nodes import nicer_partial


def apply_lags(column: pd.Series, lag: int) -> Tuple[pd.Series, str]:
    """
    This function transforms passed column to get lags of it.
        Number of lags is specified in "lag" variable.

    Args:
        column (pd.Series): Column to be transformed. Must be sorted by date.
        lag (int): Number of lags.

    Returns:
        Tuple[pd.Series, str]: Modified column and prefix to be added
            to the output DF (model input).
    """
    return column.shift(lag), LAG_COL


def apply_avg_of_days(column: pd.Series, no_of_days: int) -> Tuple[pd.Series, str]:
    """
    This function generates series with average over several days using passed column.
        E.g. column with 'sales' is passed with no_of_days=7; output is column
        with average sales over last 7 days.

    Args:
        column (pd.Series): Column to be modified. Must be sorted by date.
        no_of_days (int): Over this number of days an average will be generated.

    Returns:
        Tuple[pd.Series, str]: Modified column and prefix to be added
            to the output DF (model input).
    """
    draft_arr = []
    for count, i in enumerate(column, 1):
        if count < no_of_days:
            draft_arr.append(np.nan)
        else:
            draft_arr.append(column.iloc[(count - no_of_days) : count].mean())
    return pd.Series(draft_arr), AVG_COL


def apply_time_variable(column: pd.Series, function_arg: int) -> Tuple[pd.Series, str]:
    """
    This function creates a time variable (1, 2, 3, ...) based on
        any column sorted by date.

    Args:
        column (pd.Series): Column that would be used to generate time variable.
            It may be any column. Script reads its length only.
        function_arg (int): Useless argument - does nothing and exists
        for compatibility.

    Returns:
        Tuple[pd.Series, str]: Modified column and prefix to be added
            to the output DF (model input).
    """
    return pd.Series([x for x in range(1, len(column + 1))]), TIME_VAR_COL


def convert_column_to_0_1(df: pd.DataFrame, colname: str, value_1: Any) -> pd.DataFrame:
    """
    Converts 'colname' column to a 0/1 column based on 'value_1' column.
        If 'colname' column == value_1 then new col value is 1, else it is 0.

    Args:
        df (pd.DataFrame): Df to be modified (col replaced)
        colname (str): Colname of the column to be converted to 0/1
        value_1 (Any): If column equals to this value then its new value would be 1.

    Returns:
        pd.DataFrame: Modified df (with column replaced to 0/1)
    """
    df.reset_index(drop=True, inplace=True)
    df[colname] = pd.Series(np.where(df[colname].values == value_1, 1, 0), df.index)
    return df


def convert_columns_to_0_1(df, colnames: List[str], value_1: Any) -> pd.DataFrame:
    """
    Converts 'colnames' columns to a 0/1 column based on 'value_1' column.
        If 'colname' column == value_1 then new col value is 1, else it is 0.


    Args:
        df (pd.DataFrame): Df to be modified (cols replaced)
        colname (str): Colname of the columns to be converted to 0/1
        value_1 (Any): If column equals to this value then its new value would be 1.

    Returns:
        pd.DataFrame: Modified df (with columns replaced to 0/1)
    """
    reduce(nicer_partial(convert_column_to_0_1, value_1=value_1), colnames, df)
    return df


def convert_datetime_to_unix(df: pd.DataFrame) -> pd.DataFrame:
    """
    Convert the 'timestamp' column of the DataFrame from datetime to Unix timestamp
    format.

    Parameters:
    df (pd.DataFrame): Input DataFrame. Must have a 'timestamp' column with datetime
    values.

    Returns:
    df (pd.DataFrame): The same DataFrame with 'timestamp' converted to Unix time.
    """
    df["timestamp"] = df["timestamp"].apply(lambda x: int(time.mktime(x.timetuple())))
    return df


def add_lag(
    df: pd.DataFrame, lag_col_name: str = Y_COL, lag_days: int = 1
) -> pd.DataFrame:
    """
    Adds a new column to the dataframe with its lagged values.

    Args:
        df: The original dataframe.
        lag_col_name: The name of the column to create a lag for.
        lag_days: The number of days to lag.

    Returns:
        A dataframe with a new column containing the lagged values.
    """
    df[f"{lag_col_name}_lag"] = df[lag_col_name].shift(lag_days)
    return df
