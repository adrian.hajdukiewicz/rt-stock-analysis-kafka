import logging
from collections import Counter
from typing import Tuple, Union

import numpy as np
import pandas as pd
from numpy.random import randint

from bachelor_analysis.pipelines.consts import Y_COL


def random_train_test_split(
    data_set: pd.DataFrame, share_of_train_data: int = 0.8
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    This function splits data set into one train and one test data set.
        Random split.

    Args:
        data_set (pd.DataFrame): Data set to be divided into train/test. Must contain
            Y_COL (named like the const) and explanatory variables.
        share_of_train_data (int): determines proportions of the split. Defaults to
            0.8 and that means 20% is the test data and 80% is train.


    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: train data set, test data set
    """
    msk = np.random.rand(len(data_set)) < share_of_train_data

    train_data_set = data_set[msk]
    test_data_set = data_set[~msk]

    return train_data_set, test_data_set


def get_indices_of_random_rows(
    df_: pd.DataFrame, percent_of_rows_to_select: Union[int, float]
) -> pd.core.indexes.numeric.Int64Index:
    """
    Returns indices of random rows - they may be used e.g. with pd.drop method.
        There is {percent_of_rows_to_select}% of rows returned.

    percent_of_rows_to_select: e.g. 20 (in that case 20% of rows would be
        selected (their indices returned))

    Returns:
        pd.core.indexes.numeric.Int64Index: indices of randomly selected rows
    """
    df = df_.copy()
    # does not matter if column already exists - original df remains unchanged:
    TEMP_COL = "TO_DROP_TEMP_COL"
    promile_rows_to_drop = percent_of_rows_to_select * 10
    df[TEMP_COL] = randint(0, 1001, df.shape[0])
    return df[df[TEMP_COL] <= promile_rows_to_drop].index


def error_checks_drop_rows_to_get_balanced_y(minimum_share_of_positive_class: float):
    """
    Error checks for drop_rows_to_get_balanced_y function.

    Args:
        minimum_share_of_positive_class (float): Drops would be dropped within negative
            class to get share of positive class (1) within 'y_col' at least on
            this level.
    """
    if not isinstance(minimum_share_of_positive_class, (int, float)):
        raise ValueError(
            "'minimum_share_of_positive_class' must be 'int' or 'float' "
            "while it is of type {type(minimum_share_of_positive_class)}."
        )
    if not 0 <= minimum_share_of_positive_class <= 1:
        raise ValueError(
            "'minimum_share_of_positive_class' argument is not in (0;1) range."
        )


def drop_rows_to_get_balanced_y(
    df: pd.DataFrame, minimum_share_of_positive_class: float, y_col=Y_COL
) -> pd.DataFrame:
    """
    Works with 0-1 y columns.

    Args:
        df (pd.DataFrame): DF where y_col contains only 0/1 values (type: int).
        minimum_share_of_positive_class (float): Drops would be dropped within negative
            class to get share of positive class (1) within 'y_col' at least on
            this level.
        y_col (_type_, optional): Y col. Based on it rows would be dropped.
            Defaults to Y_COL.

    Returns:
        pd.DataFrame: Input dataframe with rows dropped to assure that share
            of positive class within 'y_col' is on at least
            'minimum_share_of_positive_class' level.
    """
    error_checks_drop_rows_to_get_balanced_y(minimum_share_of_positive_class)

    counts = Counter(df[y_col])
    share_of_pos_class = counts[1] / len(df)

    if share_of_pos_class < minimum_share_of_positive_class:
        desired_len_of_df = counts[1] / minimum_share_of_positive_class
        no_of_0class_rows_to_drop = len(df) - desired_len_of_df
        if no_of_0class_rows_to_drop >= counts[0]:
            raise ValueError(
                f"It is not possible to drop {no_of_0class_rows_to_drop} "
                f"within negative class rows because there are only {counts[0]} "
                "rows with negative class."
            )
        # drop rows
        df_0_class_only = df[df[y_col] == 0]

        share_of_rows_to_drop_0class = no_of_0class_rows_to_drop / len(df_0_class_only)
        indices_to_drop = get_indices_of_random_rows(
            df_0_class_only, share_of_rows_to_drop_0class * 100
        )
        log = logging.getLogger(__name__)
        log.info(f"Dropping {share_of_rows_to_drop_0class} rows within negative class")
        return df.drop(indices_to_drop)

    else:
        log = logging.getLogger(__name__)
        log.info(
            "There is no need to drop any rows (share of positive class is on at "
            f"least {minimum_share_of_positive_class} level."
        )
        return df
