from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.train_test_split.nodes import (
    drop_rows_to_get_balanced_y,
    random_train_test_split,
)

single_test_train = Pipeline(
    [
        node(
            func=drop_rows_to_get_balanced_y,
            inputs=[
                "data_with_dummies_n_0_1",
                "params:minimum_share_of_positive_class",
            ],
            outputs="data_balanced_y",
        ),
        node(
            func=random_train_test_split,
            inputs=[
                "data_balanced_y",
            ],
            outputs=["tr_data_temp", "tt_data_temp"],
        ),
    ]
)
