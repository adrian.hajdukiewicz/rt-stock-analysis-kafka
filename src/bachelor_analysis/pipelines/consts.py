Y_COL = "close"
DATE_COL = "PLACEHOLDER_FOR_COMPATIBILITY"

PREDS_COL = "y_pred"
MAX_TICKS_PLOT = 20

# FE:
AVG_COL = "avg"
LAG_COL = "lag"
TIME_VAR_COL = "time_var"

# mlflow:
MODEL_TYPE_TAG = "model_type"

YES_VALUE = "yes"
