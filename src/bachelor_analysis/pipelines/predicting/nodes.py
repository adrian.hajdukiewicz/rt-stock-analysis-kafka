import logging
from typing import Tuple, Union

import mlflow
import numpy as np
import pandas as pd

from bachelor_analysis.pipelines.consts import Y_COL
from bachelor_analysis.pipelines.preprocessing.nodes import drop_cols


def make_decision(
    latest_price: Union[int, float], predicted_price: Union[int, float]
) -> str:
    """
    Makes a decision to buy or sell based on the latest known stock price and
    the predicted price.

    Parameters:
    latest_price (Union[int, float]): The latest known stock price.
    predicted_price (Union[int, float]): The predicted stock price.

    Returns:
    decision (str): A string indicating whether to "Buy" or "Sell".
    """
    decision = "Buy" if predicted_price > latest_price else "Sell"

    # Log decision as a tag in MLflow
    mlflow.set_tag("decision", decision)

    return decision


def get_X_and_y(data: pd.DataFrame, y_column=Y_COL) -> Tuple[pd.DataFrame, pd.Series]:
    if y_column not in data.columns:
        raise ValueError("y_column is not present in the data set")

    y = data[y_column]
    X = drop_cols([y_column], data)

    return X, y


def predict(model, data_set: pd.DataFrame) -> np.ndarray:
    """
    This function makes predictions using test dataset.

    Args:
        model (XGBRegressor): Fitted model.
        data_set (pd.DataFrame): Data set. It may contain X and y columns
            or only X columns.

    Returns:
        np.ndarray: Predictions.
    """
    X = data_set
    predictions = model.predict(X)

    # Log predictions
    logging.getLogger(__name__).info(f"Predictions: {predictions}")

    return (predictions,)
