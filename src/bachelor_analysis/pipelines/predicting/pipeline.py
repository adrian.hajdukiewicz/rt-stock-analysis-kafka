from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.predicting.nodes import make_decision, predict

predicting = Pipeline(
    [
        node(
            func=predict,
            inputs=[
                "fitted_model",
                "prediction_input",
            ],
            outputs=["predictions"],
        ),
        node(
            func=make_decision,
            inputs={
                "predicted_price": "predictions",
                "latest_price": "latest_price",
            },
            outputs="decision",
        ),
    ]
)
