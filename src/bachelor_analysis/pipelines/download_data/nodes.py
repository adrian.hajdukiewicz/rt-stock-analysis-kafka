import json
from typing import List, Optional, Union

import pandas as pd
import requests
from kafka import KafkaConsumer, TopicPartition


def fetch_stock_data(symbol: str, apikey: str) -> pd.DataFrame:
    """
    Fetch stock data from Alpha Vantage and return it as a DataFrame.

    Parameters:
    symbol (str): The stock symbol to fetch data for.
    apikey (str): The API key for Alpha Vantage.

    Returns:
    df (pd.DataFrame): A DataFrame containing the stock data with 'timestamp' and 'close' columns. # noqa
    """
    # Define the URL
    url = f"https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={symbol}&interval=5min&apikey={apikey}"  # noqa

    # Send the request and get the data
    r = requests.get(url)
    data = r.json()

    # Extract 'Time Series (5min)' from data
    time_series_data = data["Time Series (5min)"]

    # Prepare the list of dictionaries for DataFrame
    data_list = [
        {"timestamp": timestamp, "close": values["4. close"]}
        for timestamp, values in time_series_data.items()
    ]

    # Create DataFrame
    df = pd.DataFrame(data_list)

    # Convert 'timestamp' column to datetime
    df["timestamp"] = pd.to_datetime(df["timestamp"])

    # Convert 'close' column to float
    df["close"] = df["close"].astype(float)

    return df


def create_stock_dataframe(data_dict: str) -> pd.DataFrame:
    """
    Creates a DataFrame from provided stock data. This data can be a message
    from a Kafka stream.

    It is meant for predicting. Adds 1 5 min interval to the timestamp.

    Parameters:
    data_dict (str): A dictionary with 'timestamp'
                                              and 'close' keys. For example:
                                              {"timestamp": "2023-05-28 09:30:00",
                                                "close": "100.00"}

    Returns:
    df (pd.DataFrame): A DataFrame containing the stock data with 'timestamp'
                       and 'close' columns.
    """
    # Convert data_dict["timestamp"] to a dictionary from string
    data_dict = json.loads(data_dict)

    # Add 5 minutes to the timestamp
    data_dict["timestamp"] = pd.to_datetime(data_dict["timestamp"]) + pd.Timedelta(
        minutes=5
    )

    # Create DataFrame
    df = pd.DataFrame([data_dict])

    # Convert 'timestamp' column to datetime
    df["timestamp"] = pd.to_datetime(df["timestamp"])

    # Convert 'close' column to float
    df["close"] = df["close"].astype(float)

    # rename close to close_lag
    df.rename(columns={"close": "close_lag"}, inplace=True)

    return df


def consume_last_message_from_kafka(
    bootstrap_servers: Union[str, List[str]], topic: str, partition: int = 0
) -> Optional[str]:
    """
    Consume the last message from a Kafka topic.

    Parameters:
    bootstrap_servers (str or list of str): The Kafka server addresses.
    topic (str): The Kafka topic to consume from.
    partition (int, optional): The partition to consume from. Default is 0.

    Returns:
    str or None: The last message from the Kafka topic, or None if there are
        no messages.
    """
    consumer = KafkaConsumer(
        bootstrap_servers=bootstrap_servers,
        auto_offset_reset="earliest",
        enable_auto_commit=False,
    )
    tp = TopicPartition(topic, partition)

    # Get the last offset for the topic
    consumer.assign([tp])
    end_offsets = consumer.end_offsets([tp])
    last_offset = end_offsets[tp]

    # Seek to the last offset minus one (since the offsets are 0-indexed)
    message_value = None
    if last_offset > 0:  # There are messages in the topic
        consumer.seek(tp, last_offset - 1)

        # Read the last message
        for message in consumer:
            message_value = message.value.decode("utf-8")
            break  # Only process a single message

    consumer.close()

    return message_value


def get_latest_price(df: pd.DataFrame) -> Union[int, float]:
    """
    Extracts the latest known stock price from the output DataFrame of the
    'create_stock_dataframe()' function.

    Parameters:
    df (pd.DataFrame): The DataFrame from which to extract the latest known
                       stock price. It should contain a 'close_lag' column.

    Returns:
    latest_price (Union[int, float]): The latest known stock price.
    """
    # Ensure the DataFrame has a 'close_lag' column
    if "close_lag" not in df.columns:
        raise ValueError("The DataFrame should contain a 'close_lag' column.")

    # Extract the 'close' price from the last row of the DataFrame
    latest_price = df["close_lag"].iloc[-1]

    return latest_price
