from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.download_data.nodes import (
    consume_last_message_from_kafka,
    create_stock_dataframe,
    get_latest_price,
)
from bachelor_analysis.pipelines.feature_engineering.nodes import (
    convert_datetime_to_unix,
)

download_current_data_from_kafka = Pipeline(
    [
        node(
            func=consume_last_message_from_kafka,
            inputs={
                "bootstrap_servers": "params:bootstrap_servers",
                "topic": "params:topic",
            },
            outputs="latest_msg",
        ),
        node(
            func=create_stock_dataframe,
            inputs="latest_msg",
            outputs="raw_prediction_input",
        ),
        node(
            func=convert_datetime_to_unix,
            inputs={
                "df": "raw_prediction_input",
            },
            outputs="prediction_input",
        ),
        node(
            func=get_latest_price,
            inputs="prediction_input",
            outputs="latest_price",
        ),
    ]
)
