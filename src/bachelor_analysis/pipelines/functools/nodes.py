from functools import partial, update_wrapper
from typing import Callable


def nicer_partial(f: Callable, *args, **kwargs) -> Callable:
    """
    Works the same way as `functools.partial`, but the name of the function is
    inherited from `f`.

    Args:
        f (Callable): Funtion to partialize.

    Returns:
        Calalble: Partialized function.
    """
    f_partial = partial(f, *args, **kwargs)
    return update_wrapper(f_partial, f)
