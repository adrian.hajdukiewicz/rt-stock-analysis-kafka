import datetime
from functools import reduce
from typing import Union

import numpy as np
import pandas as pd


def data_set_spine(
    col_names: list,
    start_date: datetime.date,
    end_date: datetime.date,
    start_date_delta: int,
    **l_columns: Union[pd.DataFrame, list, tuple],
) -> pd.DataFrame:
    # to generate lags with more data:
    start_date_adjusted = start_date - datetime.timedelta(start_date_delta)

    dates = pd.date_range(start=start_date_adjusted, end=end_date).to_frame()

    dfs = [
        pd.DataFrame(x) if not isinstance(x, pd.core.frame.DataFrame) else x
        for x in l_columns.values()
    ]

    def cross_join(left, right) -> pd.DataFrame:
        return left.merge(right, how="cross")

    spine = reduce(cross_join, [dates] + dfs)
    spine.columns = col_names

    return spine


# looking for rows where at least one column contains a null value
def count_missing_values(df, columns_to_exclude):
    data_without_excluded_cols = df.drop(columns_to_exclude, axis=1)

    # True if any column within a row contains a missing value:
    any_nan_in_row_excld_cols = data_without_excluded_cols.apply(
        lambda x: True if x.isnull().values.any() else False, axis=1
    )
    no_of_missing_rows_excld = any_nan_in_row_excld_cols.value_counts()

    try:
        number_rows_with_nan = no_of_missing_rows_excld[True]
    except KeyError:
        number_rows_with_nan = 0

    try:
        number_rows_without_nan = no_of_missing_rows_excld[False]
    except KeyError:
        number_rows_without_nan = 0

    print(
        "Rows that contain at least one Nan, excluding columns", columns_to_exclude, ":"
    )
    print(number_rows_with_nan)
    print(f"Rows without nan: {number_rows_without_nan}")
    percent_missing = number_rows_with_nan / (
        number_rows_with_nan + number_rows_without_nan
    )
    print(f"Percent of rows with at least one nan: {percent_missing}")


def get_unique_col_values(df: pd.DataFrame, colname: str) -> np.ndarray:
    """
    Returns unique values of a selected column from a dataframe.

    Args:
        df (pd.DataFrame): Source dataframe.
        colname (str): Colname - its unique values would be returned.

    Returns:
        np.ndarray: unique values of a selected column from a dataframe.
    """
    return tuple(df[colname].unique())
