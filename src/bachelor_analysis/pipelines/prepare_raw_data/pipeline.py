from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.download_data.nodes import fetch_stock_data

prepare_raw_data_simple_SQL = Pipeline(
    [
        # cleaning performed in notebook (data_cleaning.ipnyb)
        node(
            func=fetch_stock_data,
            inputs=["params:topic", "params:api_key"],
            outputs="raw_data",
        ),
    ]
)

prepare_sample_of_a_data_set = Pipeline(
    [
        node(
            func=lambda x: x.sample(frac=0.03),
            inputs=["raw_retail_data_set"],
            outputs="raw_data",
        ),
    ]
)
