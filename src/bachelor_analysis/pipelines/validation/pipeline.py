from kedro.pipeline import Pipeline, node
from sklearn.metrics import (
    accuracy_score,
    average_precision_score,
    f1_score,
    log_loss,
    precision_score,
    recall_score,
    roc_auc_score,
)

from bachelor_analysis.pipelines.functools.nodes import nicer_partial
from bachelor_analysis.pipelines.validation.nodes import calc_metrics

metrics_func_classification = [
    accuracy_score,
    average_precision_score,
    f1_score,
    log_loss,
    precision_score,
    recall_score,
    roc_auc_score,
]
validation = Pipeline(
    [
        node(
            func=nicer_partial(calc_metrics, metrics_func=metrics_func_classification),
            inputs=["tt_data_preproc", "predictions"],
            outputs="metrics",
        ),
    ]
)
