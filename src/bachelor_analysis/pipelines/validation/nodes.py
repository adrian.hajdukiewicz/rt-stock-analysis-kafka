from typing import Any, Callable, Dict, Tuple

import numpy as np
import pandas as pd

from bachelor_analysis.pipelines.predicting.nodes import get_X_and_y


def calc_metrics(
    data_set: pd.DataFrame, y_pred: np.ndarray, metrics_func: Tuple[Callable, ...]
) -> Dict[str, Dict[str, Any]]:
    """
    This function calculates metrics (for a single model - not quantiles) for regression
        and returns them as a dict.

    Args:
        data_set (pd.DataFrame): Whole test data set.
        y_pred (np.ndarray): Predictions.

    Returns:
        Dict[str, Dict[str, Any]]: Metrics {name_of_metric: {"value": value, "step": }
            in ML Flow format (can be logged to it directly).
    """
    y_true = get_X_and_y(data_set)[1]
    d_metrics = {}

    for m in metrics_func:
        d_metrics[m.__name__] = {"value": m(y_true, y_pred), "step": 0}

    return d_metrics
