from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.plots.nodes import (
    historical_sales_preds_vs_date_plot,
    plot_feature_importance,
)

plots = Pipeline(
    [
        node(
            func=historical_sales_preds_vs_date_plot,
            inputs=["tt_data_preproc_with_date", "predictions_with_date_col"],
            outputs="plots_sales_true_preds_vs_date",
        ),
    ]
)

plots_classification = Pipeline(
    [
        node(
            func=plot_feature_importance,
            inputs=["fitted_model"],
            outputs="plot_feature_importance",
        ),
    ]
)
