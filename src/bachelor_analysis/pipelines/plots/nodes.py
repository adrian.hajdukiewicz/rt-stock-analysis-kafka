import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from xgboost import XGBClassifier, plot_importance

from bachelor_analysis.pipelines.consts import (
    DATE_COL,
    MAX_TICKS_PLOT,
    PREDS_COL,
    Y_COL,
)


def historical_sales_preds_vs_date_plot(
    tt_data_set: pd.DataFrame, preds: pd.DataFrame
) -> plt:
    """
    This function creates a plot with historical and predicted sales
        for each quantile.

    Args:
        tt_data_set (pd.DataFrame): Test data set. Must contain DATE_COL
            and Y_COL.
        preds (pd.DataFrame): Predictions. Must contain columns:
            DATE_COL, QUANTILE_COL, PREDS_COL.

    Returns:
        plt: Object that can be logged to ML Flow using matplotlib writer.
    """
    tt_data_set.sort_values(DATE_COL, inplace=True)
    preds.sort_values([DATE_COL], inplace=True)

    # create a new figure as a substitute for deepcopy (for dictify/listify)
    plt.figure()

    # test if DATE in preds = DATE in tt_data_set
    plt.style.use("ggplot")
    plt.plot(
        tt_data_set[DATE_COL],
        tt_data_set[Y_COL],
        label="TRUE VALUES",
        linewidth=4,
        color="black",
    )

    # for q in preds[QUANTILE_COL].unique():
    # temp_preds = preds[preds[QUANTILE_COL] == q]
    # plt.plot(temp_preds[DATE_COL], temp_preds[PREDS_COL], label=f"Q_{q}")
    plt.plot(preds[DATE_COL], preds[PREDS_COL], label=PREDS_COL)
    plt.legend(loc="upper right", framealpha=0.4, fontsize="small")
    plt.xlabel("DATE", fontsize=14)
    plt.ylabel("SALES IN UNITS", fontsize=14)
    plt.title("TRUE AND PREDICTED SALES", fontsize=16)
    plt.xticks(fontsize=8)

    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d"))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.gcf().autofmt_xdate()
    plt.gcf().set_size_inches(11, 6)

    if len(tt_data_set) < MAX_TICKS_PLOT:
        no_ticks = len(tt_data_set)
    else:
        no_ticks = MAX_TICKS_PLOT
    plt.gca().xaxis.set_major_locator(plt.MaxNLocator(no_ticks))

    # return current figure only
    return plt.gcf()


def plot_feature_importance(model: XGBClassifier) -> plt:
    # figure(figsize=(20, 6), dpi=80)
    plt.rcParams["figure.figsize"] = (8, 8)
    plot_importance(model)
    plt.tight_layout()

    return plt
