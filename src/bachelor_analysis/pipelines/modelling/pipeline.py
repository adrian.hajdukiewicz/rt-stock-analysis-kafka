from kedro.pipeline import Pipeline, node

from bachelor_analysis.pipelines.modelling.nodes import define_model, fit_model

modelling = Pipeline(
    [
        node(
            func=define_model,
            inputs=None,
            outputs="model",
        ),
        node(
            func=fit_model,
            inputs=["model", "df_lag"],
            outputs="fitted_model",
        ),
    ]
)
