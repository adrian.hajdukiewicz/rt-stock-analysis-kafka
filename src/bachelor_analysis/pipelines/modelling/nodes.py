# from lightgbm import LGBMClassifier
# from sklearn.linear_model import LogisticRegression
import logging
from typing import Any

import pandas as pd
from mlflow import log_param
from xgboost import XGBRegressor

from bachelor_analysis.pipelines.consts import MODEL_TYPE_TAG, Y_COL
from bachelor_analysis.pipelines.predicting.nodes import get_X_and_y
from bachelor_analysis.pipelines.preprocessing.nodes import drop_cols


def get_X(data: pd.DataFrame, y_column=Y_COL) -> pd.DataFrame:
    """
    This function returns X from the data set -
        ready input for the model.
        Last step before feeding into the model.
        It drops y_column but will not raise an error (only warning)
        if this column is not present in the data set.

    Args:
        data (pd.DataFrame): Data set with X and (optional) y columns
            (should contain these columns only).
        y_column (str, optional): Column name with y. Defaults to Y_COL.

    Returns:
        pd.DataFrame: X - input for the model.
    """
    return drop_cols([y_column], data)


def fit_model(model: Any, tr_data: pd.DataFrame) -> Any:
    """
    This function fits models (including preprocessing).

    Args:
        pipeline (XGBRegressor): Model, not fitted.
        tr_data (pd.DataFrame): Train data set (X and y columns).
        tt_data (pd.DataFrame): Test data set (X and y columns).

    Returns:
        XGBRegressor: Model (not fitted).
    """
    X_train, y_train = get_X_and_y(tr_data)

    log = logging.getLogger(__name__)
    log.info(model.fit(X_train, y_train))
    log_param(key=MODEL_TYPE_TAG, value=type(model))

    return model


def define_model() -> XGBRegressor:
    """
    This function defines a model in sklearn pipeline.

    Returns:
        XGBRegressor: Model, not fitted
    """
    return XGBRegressor()
